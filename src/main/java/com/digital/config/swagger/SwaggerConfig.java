package com.digital.config.swagger;

import org.springdoc.core.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SwaggerConfig {
    @Bean
    public GroupedOpenApi adminApi() {
        return GroupedOpenApi.builder()
                .packagesToScan("com.digital.controller")
                .pathsToMatch("/**")
                .group("Admin")
                .build();
    }
}
