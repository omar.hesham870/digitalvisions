package com.digital.controller;

import com.digital.data.dto.AppointmentFilterDto;
import com.digital.data.dto.AppointmentReqDto;
import com.digital.data.dto.AppointmentResDto;
import com.digital.data.dto.PageDto;
import com.digital.data.ref.ReasonRef;
import com.digital.service.AppointmentService;
import com.digital.util.Constant;
import com.digital.util.Response;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(Constant.API + "/admin")
@Tag(name = "Admin", description = "Admin Apis")
@RequiredArgsConstructor
public class AdminController {

    private final AppointmentService appointmentService;

    @PostMapping("appointments")
    @Operation(summary = "create appointment")
    public ResponseEntity<Response<Boolean>> createAppointment(@RequestBody AppointmentReqDto appointment) {
        return appointmentService.createAppointment(appointment);
    }

    @PutMapping("appointments/{id}/cancel")
    @Operation(summary = "cancel appointment")
    public ResponseEntity<Response<Boolean>> cancelAppointment(@PathVariable Long id, @RequestParam ReasonRef reason) {
        return appointmentService.cancelAppointment(id, reason);
    }

    @PostMapping("appointments/{pageNumber}")
    @Operation(summary = "view all appointments with filter")
    public ResponseEntity<Response<PageDto<AppointmentResDto>>> viewAppointments(@PathVariable Integer pageNumber, @RequestBody AppointmentFilterDto filter) {
        return appointmentService.viewAppointments(pageNumber, filter);
    }

    @GetMapping("home/{pageNumber}")
    @Operation(summary = "home appointments")
    public ResponseEntity<Response<PageDto<AppointmentResDto>>> homeAppointments(@PathVariable Integer pageNumber) {
        return appointmentService.homeAppointments(pageNumber);
    }

    @GetMapping("patients/{patientId}/appointments/{pageNumber}")
    @Operation(summary = "view patient appointment history")
    public ResponseEntity<Response<PageDto<AppointmentResDto>>> patientHistory(@PathVariable Integer pageNumber, @PathVariable Long patientId) {
        return appointmentService.patientHistory(pageNumber, patientId);
    }

}
