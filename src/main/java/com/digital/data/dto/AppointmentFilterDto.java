package com.digital.data.dto;

import com.digital.util.Constant;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.deser.std.DateDeserializers;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class AppointmentFilterDto implements Serializable {
    private String patientName;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constant.DATE_FORMAT)
    @JsonDeserialize(using = DateDeserializers.DateDeserializer.class, as = Date.class)
    @Schema(type = "string", pattern = Constant.DATE_FORMAT)
    private Date date;
    private Boolean isFuture;
    private Boolean isHistory;
    private Long patientId;
    @JsonIgnore
    private Integer pageNumber;
}
