package com.digital.data.dto;

import com.digital.util.Constant;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
public class AppointmentResDto implements Serializable {
    private Long id;
    private String reason;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constant.DATE_TIME_FORMAT)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class, as = LocalDateTime.class)
    @Schema(type = "string", pattern = Constant.DATE_TIME_FORMAT)
    private LocalDateTime reserveDate;
    private Long statusId;
    private String status;
    private String cancelReason;
    private PatientResDto patient;

    @Data
    public static class PatientResDto implements Serializable {
        private Long id;
        private String fullName;
        private String email;
        private String mobileNumber;
        private String nationalId;
    }
}
