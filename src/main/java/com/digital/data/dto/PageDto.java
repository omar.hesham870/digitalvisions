package com.digital.data.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.List;


@Data
public class PageDto<T> implements Serializable {
    List<T> content;
    int totalPages = 0;
    Long totalElements = 0L;
}
