package com.digital.data.ref;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum ReasonRef {
    noShow("عدم الحضور"), patientRequest("بناء على طلب المريض"), physicianApology("اعتذار الطبيب");

    private final String name;
}
