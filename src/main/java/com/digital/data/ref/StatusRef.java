package com.digital.data.ref;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum StatusRef {
    New(1), canceled(2), completed(3);

    private final long id;
}
