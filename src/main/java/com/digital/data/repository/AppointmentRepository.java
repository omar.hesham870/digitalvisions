package com.digital.data.repository;

import com.digital.data.domain.Appointment;
import com.digital.data.dto.AppointmentFilterDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface AppointmentRepository extends JpaRepository<Appointment,Long> {

    @Query("select ap from Appointment ap where " +
            "(:#{#input.date} is null or date(ap.reserveDate) = :#{#input.date}) " +
            "and (:#{#input.isFuture} is null or :#{#input.isFuture} = false or date(ap.reserveDate) >= current_date()) " +
            "and (:#{#input.isHistory} is null or :#{#input.isHistory} = false or date(ap.reserveDate) < current_date()) " +
            "and (:#{#input.patientId} is null or :#{#input.patientId} = 0L  or ap.patient.id = :#{#input.patientId}) " +
            "and (:#{#input.patientName} is null or ap.patient.fullName = :#{#input.patientName})")
    Page<Appointment> findAllAppointments(AppointmentFilterDto input, Pageable pageable);
}
