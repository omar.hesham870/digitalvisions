package com.digital.data.repository;

import com.digital.data.domain.AppointmentStatus;
import com.digital.data.domain.Patient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PatientRepository extends JpaRepository<Patient,Long> {
}
