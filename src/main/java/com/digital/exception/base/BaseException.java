package com.digital.exception.base;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.http.HttpStatus;

@Data
@EqualsAndHashCode(callSuper = false)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BaseException extends RuntimeException {

    private HttpStatus status = HttpStatus.BAD_REQUEST;
    protected String module;
    protected String debugMessage;
    protected String message;
    protected String errorCode;


    public BaseException(Throwable cause) {
        super(cause);
    }

    public BaseException(String message) {
        super(message);
        this.message = message;
    }

    public BaseException(HttpStatus status, String message, Throwable cause) {
        super(message, cause);
        this.status = status;
        this.message = message;
    }

    public BaseException(HttpStatus status, String message, String errorCode, Throwable cause) {
        super(message, cause);
        this.status = status;
        this.message = message;
        this.errorCode = errorCode;
    }

    public BaseException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, String module, String fullDebugMessage) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.module = module;
        this.debugMessage = fullDebugMessage;
    }


    public BaseException(Throwable cause, String module, String fullDebugMessage, String message) {
        super(cause);
        this.debugMessage = fullDebugMessage;
        this.module = module;
        this.message = message;
    }


    public BaseException(HttpStatus status, String message, String fullDebugMessage) {
        super(message);
        this.status = status;
        this.debugMessage = fullDebugMessage;
        this.message = message;
    }

    public BaseException(HttpStatus status, String message) {
        super(message);
        this.status = status;
        this.message = message;
    }


}
