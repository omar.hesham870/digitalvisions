package com.digital.exception.custom;

import org.apache.commons.lang3.StringUtils;

public class EmailAlreadyExistException extends RuntimeException {

    public EmailAlreadyExistException(Class<Object> clazz, String mail) {
        super(StringUtils.capitalize(clazz.getSimpleName()) + " : " + mail);
    }
}
