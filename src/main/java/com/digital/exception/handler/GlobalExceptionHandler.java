package com.digital.exception.handler;


import com.digital.exception.base.BaseException;
import com.digital.exception.custom.EmailAlreadyExistException;
import com.digital.exception.custom.EntityFoundException;
import com.digital.exception.custom.EntityNotFoundException;
import com.digital.exception.util.ErrorDebugCause;
import com.digital.util.ApiError;
import com.digital.util.Response;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.util.net.openssl.ciphers.Authentication;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.nio.file.AccessDeniedException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.springframework.http.HttpStatus.*;

@SuppressWarnings("ALL")
@RestControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
@RequiredArgsConstructor
@Slf4j
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    private final EntityManager entityManager;

    private static Boolean enableAuditing;


    @Value("${app.error.auditing-enable:true}")
    private void setEnableAuditing(Boolean value) {
        enableAuditing = value;
    }

    @Value("${schema.name}")
    private String schemaName;

    /**
     * Handle MissingServletRequestParameterException. Triggered when a 'required' request parameter is missing.
     *
     * @param ex      MissingServletRequestParameterException
     * @param headers HttpHeaders
     * @param status  HttpStatus
     * @param request WebRequest
     * @return the BaseException object
     */
    @Override
    protected ResponseEntity<Object> handleMissingServletRequestParameter(
            MissingServletRequestParameterException ex, HttpHeaders headers,
            HttpStatus status, WebRequest request) {
        return buildResponseEntity(new BaseException(BAD_REQUEST, "Parameter is missing", ex));
    }


    /**
     * Handle HttpMediaTypeNotSupportedException. This one triggers when JSON is invalid as well.
     *
     * @param ex      HttpMediaTypeNotSupportedException
     * @param headers HttpHeaders
     * @param status  HttpStatus
     * @param request WebRequest
     * @return the BaseException object
     */
    @Override
    protected ResponseEntity<Object> handleHttpMediaTypeNotSupported(
            HttpMediaTypeNotSupportedException ex,
            HttpHeaders headers,
            HttpStatus status,
            WebRequest request) {

        StringBuilder builder = new StringBuilder();
        builder.append(ex.getContentType());
        builder.append("Unsupported media type");
        ex.getSupportedMediaTypes().forEach(t -> builder.append(t).append(", "));
        return buildResponseEntity(new BaseException(HttpStatus.UNSUPPORTED_MEDIA_TYPE, builder.substring(0, builder.length() - 2), ex));
    }

    /**
     * Handle MethodArgumentNotValidException. Triggered when an object fails @Valid validation.
     *
     * @param ex      the MethodArgumentNotValidException that is thrown when @Valid validation fails
     * @param headers HttpHeaders
     * @param status  HttpStatus
     * @param request WebRequest
     * @return the BaseException object
     */
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            MethodArgumentNotValidException ex,
            HttpHeaders headers,
            HttpStatus status,
            WebRequest request) {
        BaseException BaseException = new BaseException(EXPECTATION_FAILED, "Validation error", ex);
        return buildResponseEntity(BaseException);
    }

    /**
     * Handles javax.validation.ConstraintViolationException. Thrown when @Validated fails.
     *
     * @param ex the ConstraintViolationException
     * @return the BaseException object
     */
    @ExceptionHandler(javax.validation.ConstraintViolationException.class)
    protected ResponseEntity<Object> handleConstraintViolation(
            javax.validation.ConstraintViolationException ex) {
        BaseException BaseException = new BaseException(BAD_REQUEST, "Validation error", ex);
        return buildResponseEntity(BaseException);
    }

    /**
     * Handles EntityNotFoundException. Created to encapsulate errors with more detail than javax.persistence.EntityNotFoundException.
     *
     * @param ex the EntityNotFoundException
     * @return the BaseException object
     */
    @ExceptionHandler(EntityNotFoundException.class)
    protected ResponseEntity<Object> handleEntityNotFound(EntityNotFoundException ex) {
        String message = ex.getMessage();
        String[] splitter = message.split(":");
        message = splitter[0] + " was not found for parameters " + splitter[1];
        BaseException BaseException = new BaseException(NOT_FOUND, message, ex);
        return buildResponseEntity(BaseException);
    }

    /**
     * Handles EntityFoundException. Created to encapsulate errors with more detail than javax.persistence.EntityFoundException.
     *
     * @param ex the EntityFoundException
     * @return the BaseException object
     */
    @ExceptionHandler(EntityFoundException.class)
    protected ResponseEntity<Object> handleEntityFoundException(EntityFoundException ex) {
        String message = ex.getMessage();
        String[] splitter = message.split(":");
        message = splitter[0] + " was found for parameters " + splitter[1];
        BaseException BaseException = new BaseException(FOUND, message, ex);
        return buildResponseEntity(BaseException);
    }

    /**
     * Handles EmailAlreadyExistException. Created to encapsulate errors with more detail than javax.persistence.EntityNotFoundException.
     *
     * @param ex the EmailAlreadyExistException
     * @return the BaseException object
     */
    @ExceptionHandler(EmailAlreadyExistException.class)
    protected ResponseEntity<Object> handleEmailAlreadyExistException(EmailAlreadyExistException ex) {
        String message = ex.getMessage();
        String[] splitter = message.split(":");
        message = splitter[0] + " was found for email " + splitter[1];
        BaseException BaseException = new BaseException(FOUND, message, ex);
        return buildResponseEntity(BaseException);
    }

    /**
     * Handle HttpMessageNotReadableException. Happens when request JSON is malformed.
     *
     * @param ex      HttpMessageNotReadableException
     * @param headers HttpHeaders
     * @param status  HttpStatus
     * @param request WebRequest
     * @return the BaseException object
     */
    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        return buildResponseEntity(new BaseException(HttpStatus.BAD_REQUEST, "Malformed json request", ex));
    }

    /**
     * Handle HttpMessageNotWritableException.
     *
     * @param ex      HttpMessageNotWritableException
     * @param headers HttpHeaders
     * @param status  HttpStatus
     * @param request WebRequest
     * @return the BaseException object
     */
    @Override
    protected ResponseEntity<Object> handleHttpMessageNotWritable(HttpMessageNotWritableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        return buildResponseEntity(new BaseException(HttpStatus.INTERNAL_SERVER_ERROR, "Writing json output error", ex));
    }

    /**
     * Handle NoHandlerFoundException.
     *
     * @param ex
     * @param headers
     * @param status
     * @param request
     * @return
     */
    @Override
    protected ResponseEntity<Object> handleNoHandlerFoundException(
            NoHandlerFoundException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        BaseException BaseException = new BaseException(BAD_REQUEST, "Could not find method", ex);
        return buildResponseEntity(BaseException);
    }

    /**
     * Handle javax.persistence.EntityNotFoundException
     */
    @ExceptionHandler(javax.persistence.EntityNotFoundException.class)
    protected ResponseEntity<Object> handleEntityNotFound(javax.persistence.EntityNotFoundException ex) {
        String message = ex.getMessage();
        Pattern p = Pattern.compile("\\d+");
        Matcher m = p.matcher(message);
        if (m.find())
            message = m.group(0);
        return buildResponseEntity(new BaseException(HttpStatus.NOT_FOUND, "No data with key "+ message, ex));
    }

    /**
     * Handle org.springframework.dao.EmptyResultDataAccessException
     */
    @ExceptionHandler(EmptyResultDataAccessException.class)
    protected ResponseEntity<Object> handleEntityNotFound(EmptyResultDataAccessException ex) {
        String message = ex.getMessage();
        Pattern p = Pattern.compile("\\d+");
        Matcher m = p.matcher(message);
        if (m.find())
            message = m.group(0);
        return buildResponseEntity(new BaseException(HttpStatus.NOT_FOUND, "No data with key "+ message, ex));
    }

    /**
     * Handle DataIntegrityViolationException, inspects the cause for different DB causes.
     *
     * @param ex the DataIntegrityViolationException
     * @return the BaseException object
     */
    @ExceptionHandler(DataIntegrityViolationException.class)
    protected ResponseEntity<Object> handleDataIntegrityViolation(DataIntegrityViolationException ex, WebRequest request) {
        if (ex.getCause() instanceof ConstraintViolationException) {
            String message = ex.getCause().getCause().getMessage();
            if (message.contains("Duplicate")) {
                Pattern p = Pattern.compile("'([^']*)'");
                Matcher m = p.matcher(message);
                if (m.find())
                    message = m.group(1);
                message = "field is alraedy exist " + message;
            } else if (message.contains("null")) {
                Pattern p = Pattern.compile("'([^']*)'");
                Matcher m = p.matcher(message);
                if (m.find())
                    message = m.group(0);
                message = message+ " can not be null";
            } else if (message.contains("Cannot add or update a child row")) {
                message = message.substring(message.indexOf("REFERENCES") + 12, message.length() - 9);
                message = "id not found for "+ message;
                return buildResponseEntity(new BaseException(NOT_FOUND, message, ex));
            }

            return buildResponseEntity(new BaseException(HttpStatus.CONFLICT, message, ex));
        }

        return buildResponseEntity(new BaseException(HttpStatus.INTERNAL_SERVER_ERROR, "Database error"));
    }

    /**
     * Handle Exception, handle generic Exception.class
     *
     * @param ex the Exception
     * @return the BaseException object
     */
    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    protected ResponseEntity<Object> handleMethodArgumentTypeMismatch(MethodArgumentTypeMismatchException ex,
                                                                      WebRequest request) {
        BaseException BaseException = new BaseException(BAD_REQUEST, "Data fail",
                ex);
        return buildResponseEntity(BaseException);
    }

    @ExceptionHandler(BaseException.class)
    protected ResponseEntity<Object> handleBaseException(BaseException ex) {
        return buildResponseEntity(ex);
    }

    @ExceptionHandler(AccessDeniedException.class)
    protected ResponseEntity<Object> handleAccessDeniedException(AccessDeniedException ex) {
        BaseException BaseException = new BaseException(FORBIDDEN, "Access Denied",
                ex);
        return buildResponseEntity(BaseException);
    }

    @ExceptionHandler(Exception.class)
    protected ResponseEntity<Object> handleException(Exception ex) {
        BaseException BaseException = new BaseException(HttpStatus.INTERNAL_SERVER_ERROR, "General Error", ex);
        return buildResponseEntity(BaseException);
    }


    private ResponseEntity<Object> buildResponseEntity(BaseException BaseException) {
        ApiError apiError = ApiError.builder()
                .status(BaseException.getStatus())
                .debugCause(ErrorDebugCause.getChangeLogError(BaseException))
                .message(BaseException.getMessage())
                .module(BaseException.getModule())
                .errorCode(BaseException.getErrorCode())
                .build();
        Response response = Response.builder().error(apiError).success(false).build();

        return new ResponseEntity<>(response, BaseException.getStatus());
    }



}
