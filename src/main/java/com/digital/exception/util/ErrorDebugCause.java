package com.digital.exception.util;

public class ErrorDebugCause {

    public static String getChangeLogError(Throwable thr) {
        StringBuilder sb = new StringBuilder();

        do {
            sb.append((sb.length() > 0) ? System.lineSeparator() + "Caused by: " + thr.toString() : thr.toString()).append(System.lineSeparator());
            for (int i = 0; i < thr.getStackTrace().length; i++) {
                sb.append("\t ").append(thr.getStackTrace()[i]).append((i + 1 != thr.getStackTrace().length) ? System.lineSeparator() : "");
            }
            thr = thr.getCause();
        } while (thr != null);
        return sb.toString();
    }

}
