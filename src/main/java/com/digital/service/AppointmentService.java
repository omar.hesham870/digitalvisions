package com.digital.service;


import com.digital.data.dto.AppointmentFilterDto;
import com.digital.data.dto.AppointmentReqDto;
import com.digital.data.dto.AppointmentResDto;
import com.digital.data.dto.PageDto;
import com.digital.data.ref.ReasonRef;
import com.digital.exception.base.BaseException;
import com.digital.usecase.appointment.CancelAppointment;
import com.digital.usecase.appointment.CreateAppointment;
import com.digital.usecase.appointment.ViewAppointments;
import com.digital.util.Response;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Service
@RequiredArgsConstructor
@Transactional(rollbackFor = {BaseException.class, Exception.class})
public class AppointmentService {

    private final CreateAppointment createAppointment;
    private final CancelAppointment cancelAppointment;
    private final ViewAppointments viewAppointments;


    public ResponseEntity<Response<Boolean>> createAppointment(AppointmentReqDto appointment) {
        Response<Boolean> response = Response.<Boolean>builder()
                .successMessage("Appointment created successfully")
                .data(createAppointment.execute(appointment))
                .success(true).build();
        return ResponseEntity.ok(response);
    }

    public ResponseEntity<Response<Boolean>> cancelAppointment(Long id, ReasonRef reason) {
        Map<String, Object> input = new HashMap<>();
        input.put("id", id);
        input.put("reason", reason);
        Response<Boolean> response = Response.<Boolean>builder()
                .successMessage("Appointment cancelled successfully")
                .data(cancelAppointment.execute(input))
                .success(true).build();
        return ResponseEntity.ok(response);
    }


    public ResponseEntity<Response<PageDto<AppointmentResDto>>> viewAppointments(Integer pageNumber, AppointmentFilterDto filter) {
        filter.setPageNumber(pageNumber);
        Response<PageDto<AppointmentResDto>> response = Response.<PageDto<AppointmentResDto>>builder()
                .successMessage("Appointments retrieved successfully")
                .data(viewAppointments.execute(filter))
                .success(true).build();
        return ResponseEntity.ok(response);
    }

    public ResponseEntity<Response<PageDto<AppointmentResDto>>> homeAppointments(Integer pageNumber) {
        AppointmentFilterDto filter = new AppointmentFilterDto();
        filter.setDate(new Date());
        filter.setPageNumber(pageNumber);
        Response<PageDto<AppointmentResDto>> response = Response.<PageDto<AppointmentResDto>>builder()
                .successMessage("Appointments retrieved successfully")
                .data(viewAppointments.execute(filter))
                .success(true).build();
        return ResponseEntity.ok(response);
    }

    public ResponseEntity<Response<PageDto<AppointmentResDto>>> patientHistory(Integer pageNumber, Long patientId) {
        AppointmentFilterDto filter = new AppointmentFilterDto();
        filter.setPatientId(patientId);
        filter.setPageNumber(pageNumber);
        Response<PageDto<AppointmentResDto>> response = Response.<PageDto<AppointmentResDto>>builder()
                .successMessage("Appointments retrieved successfully")
                .data(viewAppointments.execute(filter))
                .success(true).build();
        return ResponseEntity.ok(response);
    }
}
