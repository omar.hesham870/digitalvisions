package com.digital.usecase.appointment;

import com.digital.data.domain.Appointment;
import com.digital.data.dto.AppointmentReqDto;
import com.digital.data.ref.ReasonRef;
import com.digital.data.ref.StatusRef;
import com.digital.data.repository.AppointmentRepository;
import com.digital.data.repository.AppointmentStatusRepository;
import com.digital.data.repository.PatientRepository;
import com.digital.exception.base.BaseException;
import com.digital.exception.custom.EntityNotFoundException;
import com.digital.util.UseCase;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
@RequiredArgsConstructor
public class CancelAppointment implements UseCase<Map<String, Object>, Boolean> {

    private final AppointmentRepository appointmentRepository;
    private final AppointmentStatusRepository appointmentStatusRepository;

    @Override
    public Boolean execute(Map<String, Object> input) {
        Long id = (Long) input.get("id");
        ReasonRef reason = (ReasonRef) input.get("reason");
        Appointment appointment = appointmentRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(Appointment.class, "id", id.toString()));

        if(appointment.getStatus().getId().equals(StatusRef.canceled.getId())
                && appointment.getCancelReason().getName().equals(reason.getName()))
            throw new BaseException("Sorry, you did this action before");

        appointment.setStatus(appointmentStatusRepository.getById(StatusRef.canceled.getId()));
        appointment.setCancelReason(reason);
        appointmentRepository.save(appointment);
        return true;
    }

}
