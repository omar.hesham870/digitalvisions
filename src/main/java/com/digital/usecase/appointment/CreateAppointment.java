package com.digital.usecase.appointment;

import com.digital.data.domain.Appointment;
import com.digital.data.dto.AppointmentReqDto;
import com.digital.data.ref.StatusRef;
import com.digital.data.repository.AppointmentRepository;
import com.digital.data.repository.AppointmentStatusRepository;
import com.digital.data.repository.PatientRepository;
import com.digital.util.UseCase;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CreateAppointment implements UseCase<AppointmentReqDto, Boolean> {

    private final AppointmentRepository appointmentRepository;
    private final PatientRepository patientRepository;
    private final AppointmentStatusRepository appointmentStatusRepository;
    private final AppointmentMapper appointmentMapper;

    @Override
    public Boolean execute(AppointmentReqDto input) {
        Appointment appointment = appointmentMapper.toAppointment(input);
        appointment.setPatient(patientRepository.getById(1L));
        appointment.setStatus(appointmentStatusRepository.getById(StatusRef.New.getId()));
        appointmentRepository.save(appointment);
        return true;
    }

    @Mapper(componentModel = "spring")
    public interface AppointmentMapper {
        Appointment toAppointment(AppointmentReqDto appointmentReqDto);
    }
}
