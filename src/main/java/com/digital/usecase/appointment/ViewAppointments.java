package com.digital.usecase.appointment;

import com.digital.data.domain.Appointment;
import com.digital.data.dto.AppointmentFilterDto;
import com.digital.data.dto.AppointmentReqDto;
import com.digital.data.dto.AppointmentResDto;
import com.digital.data.dto.PageDto;
import com.digital.data.ref.ReasonRef;
import com.digital.data.ref.StatusRef;
import com.digital.data.repository.AppointmentRepository;
import com.digital.data.repository.AppointmentStatusRepository;
import com.digital.exception.base.BaseException;
import com.digital.exception.custom.EntityNotFoundException;
import com.digital.util.Constant;
import com.digital.util.UseCase;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class ViewAppointments implements UseCase<AppointmentFilterDto, PageDto<AppointmentResDto>> {

    private final AppointmentRepository appointmentRepository;
    private final ViewAppointmentsMapper viewAppointmentsMapper;

    @Override
    public PageDto<AppointmentResDto> execute(AppointmentFilterDto input) {
        PageDto<AppointmentResDto> res = new PageDto<>();

        Page<Appointment> appointmentPage = appointmentRepository.findAllAppointments(input, PageRequest.of(input.getPageNumber(), Constant.PAGE_SIZE, Sort.by(Sort.Direction.DESC, "id")));

        res.setContent(viewAppointmentsMapper.toAppointmentListDto(appointmentPage.getContent()));
        res.setTotalElements(appointmentPage.getTotalElements());
        res.setTotalPages(appointmentPage.getTotalPages());

        return res;
    }

    @Mapper(componentModel = "spring")
    public interface ViewAppointmentsMapper {
        @Mappings({
                @Mapping(target = "statusId", source = "status.id"),
                @Mapping(target = "status", source = "status.nameAr"),
        })
        AppointmentResDto toAppointmentDto(Appointment content);
        List<AppointmentResDto> toAppointmentListDto(List<Appointment> content);
    }
}
