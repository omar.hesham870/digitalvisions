package com.digital.util;

public class Constant {
    public final static String API = "/api/v1";

    //------------------------------------------ FORMATS ------------------------------------------
    public final static String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public final static String DATE_FORMAT = "yyyy-MM-dd";

    //------------------------------------------------------------------------------------
    public static final int PAGE_SIZE = 10;

}
