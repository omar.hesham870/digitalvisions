package com.digital.util;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@Builder
public class Response<T> implements Serializable {

    ApiError error;
    private boolean success;
    private String successMessage;
    private T data;
    @Builder.Default
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constant.DATE_TIME_FORMAT)
    private LocalDateTime responseTime = LocalDateTime.now();
}
