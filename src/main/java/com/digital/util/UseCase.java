package com.digital.util;

import java.io.IOException;

public interface UseCase<I, O> {
    O execute(I input) throws IOException;
}
