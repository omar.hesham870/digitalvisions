-- PATIENT
INSERT INTO `patient`(`id`, `full_name`, `email`, `mobile_number`, `national_id`) VALUES (1, 'patient test', 'patient@gmail.com', '01271021430', '29607091405599');

-- APPOINTMENT-STATUS
INSERT INTO `appointment_status` (`id`, `name_ar`, `name_en`)VALUES ('1', 'جديد', 'New');
INSERT INTO `appointment_status` (`id`,  `name_ar`, `name_en`)VALUES ('2', 'ملغي', 'Canceled');
INSERT INTO `appointment_status` (`id`,  `name_ar`, `name_en`)VALUES ('3', 'مكتمل', 'Completed');
